# -*- coding: UTF-8 -*-

from os import listdir, remove
from os.path import isfile, isdir, join
import io
import sqlite3
import nltk
import sys
import time
from tabulate import tabulate
from bs4 import BeautifulSoup
from nltk.corpus import stopwords

# variables
db = sqlite3.connect('../inverted-index.db')
mypath = '../pages'
start = time.time()
stop_words_slovene = set(#stopwords.words("slovenian")).union(set(
        ["ter","nov","novo", "nova","zato","če", "zaradi", "a", "ali", "april", "avgust", "b", "bi", "bil", "bila", "bile", "bili", "bilo", "biti",
         "blizu", "bo", "bodo", "bojo", "bolj", "bom", "bomo", "boste", "bova", "boš", "brez", "c", "cel", "cela",
         "celi", "celo", "d", "da", "daleč", "dan", "danes", "datum", "december", "deset", "deseta", "deseti", "deseto",
         "devet", "deveta", "deveti", "deveto", "do", "dober", "dobra", "dobri", "dobro", "dokler", "dol", "dolg",
         "dolga", "dolgi", "dovolj", "drug", "druga", "drugi", "drugo", "dva", "dve", "e", "eden", "en", "ena", "ene",
         "eni", "enkrat", "eno", "etc.", "f", "februar", "g", "g.", "ga", "ga.", "gor", "gospa", "gospod", "h", "halo",
         "i", "idr.", "ii", "iii", "in", "iv", "ix", "iz", "j", "januar", "jaz", "je", "ji", "jih", "jim", "jo",
         "julij", "junij", "jutri", "k", "kadarkoli", "kaj", "kajti", "kako", "kakor", "kamor", "kamorkoli", "kar",
         "karkoli", "katerikoli", "kdaj", "kdo", "kdorkoli", "ker", "ki", "kje", "kjer", "kjerkoli", "ko", "koder",
         "koderkoli", "koga", "komu", "kot", "kratek", "kratka", "kratke", "kratki", "l", "lahka", "lahke", "lahki",
         "lahko", "le", "lep", "lepa", "lepe", "lepi", "lepo", "leto", "m", "maj", "majhen", "majhna", "majhni",
         "malce", "malo", "manj", "marec", "me", "med", "medtem", "mene", "mesec", "mi", "midva", "midve", "mnogo",
         "moj", "moja", "moje", "mora", "morajo", "moram", "moramo", "morate", "moraš", "morem", "mu", "n", "na", "nad",
         "naj", "najina", "najino", "najmanj", "naju", "največ", "nam", "narobe", "nas", "nato", "nazaj", "naš", "naša",
         "naše", "ne", "nedavno", "nedelja", "nek", "neka", "nekaj", "nekatere", "nekateri", "nekatero", "nekdo",
         "neke", "nekega", "neki", "nekje", "neko", "nekoga", "nekoč", "ni", "nikamor", "nikdar", "nikjer", "nikoli",
         "nič", "nje", "njega", "njegov", "njegova", "njegovo", "njej", "njemu", "njen", "njena", "njeno", "nji",
         "njih", "njihov", "njihova", "njihovo", "njiju", "njim", "njo", "njun", "njuna", "njuno", "no", "nocoj",
         "november", "npr.", "o", "ob", "oba", "obe", "oboje", "od", "odprt", "odprta", "odprti", "okoli", "oktober",
         "on", "onadva", "one", "oni", "onidve", "osem", "osma", "osmi", "osmo", "oz.", "p", "pa", "pet", "peta",
         "petek", "peti", "peto", "po", "pod", "pogosto", "poleg", "poln", "polna", "polni", "polno", "ponavadi",
         "ponedeljek", "ponovno", "potem", "povsod", "pozdravljen", "pozdravljeni", "prav", "prava", "prave", "pravi",
         "pravo", "prazen", "prazna", "prazno", "prbl.", "precej", "pred", "prej", "preko", "pri", "pribl.",
         "približno", "primer", "pripravljen", "pripravljena", "pripravljeni", "proti", "prva", "prvi", "prvo", "r",
         "ravno", "redko", "res", "reč", "s", "saj", "sam", "sama", "same", "sami", "samo", "se", "sebe", "sebi",
         "sedaj", "sedem", "sedma", "sedmi", "sedmo", "sem", "september", "seveda", "si", "sicer", "skoraj", "skozi",
         "slab", "smo", "so", "sobota", "spet", "sreda", "srednja", "srednji", "sta", "ste", "stran", "stvar", "sva",
         "t", "ta", "tak", "taka", "take", "taki", "tako", "takoj", "tam", "te", "tebe", "tebi", "tega", "težak",
         "težka", "težki", "težko", "ti", "tista", "tiste", "tisti", "tisto", "tj.", "tja", "to", "toda", "torek",
         "tretja", "tretje", "tretji", "tri", "tu", "tudi", "tukaj", "tvoj", "tvoja", "tvoje", "u", "v", "vaju", "vam",
         "vas", "vaš", "vaša", "vaše", "ve", "vedno", "velik", "velika", "veliki", "veliko", "vendar", "ves", "več",
         "vi", "vidva", "vii", "viii", "visok", "visoka", "visoke", "visoki", "vsa", "vsaj", "vsak", "vsaka", "vsakdo",
         "vsake", "vsaki", "vsakomur", "vse", "vsega", "vsi", "vso", "včasih", "včeraj", "x", "z", "za", "zadaj",
         "zadnji", "zakaj", "zaprta", "zaprti", "zaprto", "zdaj", "zelo", "zunaj", "č", "če", "šesto", "četrta",
         "četrtek", "četrti", "četrto", "čez", "čigav", "š", "šest", "šesta", "šesti", "šesto", "štiri", "ž", "že",
         "svoj", "jesti", "imeti","\u0161e", "iti", "kak", "www", "km", "eur", "pač", "del", "kljub", "šele", "prek",
         "preko", "znova", "morda","kateri","katero","katera", "ampak", "lahek", "lahka", "lahko", "morati", "torej"]).union(set(
             ["(", ")", '©', '-', '--', '•', ';', '.', ',', ':', '!', '–', '×', '/', '·', '<', '>', '&', '"', '=', 
             '``', '...', '€', '…', '’', '→', '☼☼', 'φ', 'λ', '%', "'", '"', '*', '✗', '✓', '☆☆☆☆☆', '★☆☆☆☆', '★★☆☆☆',
             '★★★☆☆', '★★★★☆', '★★★★★', '▼', '›', '“', '”', ' ', "'and", "'or", '***', '****' ]))

# functions
def getTextFromPath(path):
    html = io.open(path, mode="r", encoding="utf-8").read()
    soup = BeautifulSoup(html, 'lxml')
    for script in soup(["script", "style"]): 
        script.extract()
    return soup.getText(" ")

def processString(text, lower=True, stopwords=True):
    tokens = nltk.tokenize.word_tokenize(text)
    if lower and stopwords:
        return [x.lower() for x in tokens if x not in stop_words_slovene]
    elif lower and not stopwords:
        return [x.lower() for x in tokens]
    elif not lower and stopwords:
        return [x for x in tokens if x not in stop_words_slovene]
    elif not lower and not stopwords:
        return tokens

def findIndexes(substring, string):
    last_found = -1  
    while True:
        last_found = string.find(substring, last_found + 1)
        if last_found == -1:
            break
        yield last_found

def processFile(file, documentName):
    print("Processing file: " + documentName + (" "*20),end="\r", flush=True)
    tokens = processString(file)
    tokensStop = processString(file, stopwords=False)
    frequencies = {x:tokens.count(x) for x in tokens}
    for key, value in frequencies.items():
        if len(key) < 3:
            continue
        occurences = list(findIndexes(key, file.lower()))
        occurences = [str(x) for x in occurences]
        try:
            db.execute('INSERT INTO IndexWord values (?)', [key])
        except sqlite3.IntegrityError:
            pass
        try:
            db.execute('INSERT INTO Posting values (?,?,?,?)', [str(key), documentName, str(len(occurences)), ','.join(occurences)])
        except sqlite3.IntegrityError:
            pass
    db.commit()

def processResults(frequencies, formated):
    frequencies = sorted(frequencies, key=lambda x: x[0], reverse=True)
    for index, frequency in enumerate(frequencies[0:10]):
        opisi = []
        file = getTextFromPath(mypath + '/' + frequency[1])
        indexes = []
        for key in formated[frequency[1]]:
            indexes.extend(formated[frequency[1]][key][1].split(","))
        indexes = [int(ind) for ind in indexes]
        indexes = sorted(indexes)
        differences = []
        for i in range(len(indexes)-1):
            differences.append((i,indexes[i+1] - indexes[i]))
        differences = sorted(differences, key=lambda x: x[1])
        count = 0
        for diff in differences[0:10]:
            if count == 3:
                break
            try:
                if str.istitle(file[indexes[diff[0]]]):
                    opisi.append((file[indexes[diff[0]]:indexes[diff[0]]+100] + " ...").replace("\n", " ").strip())
                else:
                    opisi.append(("... " + file[indexes[diff[0]]:indexes[diff[0]]+100] + " ...").replace("\n", " ").strip())
                count += 1
            except IndexError:
                pass
            
        frequencies[index] += ("\n\n".join(opisi),)
    print(" "*50)
    print("Rezultati: \n")
    print(tabulate(frequencies[0:10], headers=['Frekvenca', 'Ime datoteke', 'Pojavitve v besedilu']))
    print("Skupno stevilo rezultatov: " + str(len(frequencies)))

action = sys.argv[1] if len(sys.argv) > 1 else None

if action == 'build-index':
    folders = [f for f in listdir(mypath) if isdir(join(mypath, f))]
    for folder in folders:
        files = [file for file in listdir(mypath + '/' + folder)]
        for file in files:
            if ".html" in file:
                processFile(getTextFromPath(mypath + '/' + folder + '/' + file), folder + '/' + file)
    print(" "*50)
    print("Finished creating index.")
elif action == 'build-db':
    db.execute("""
        CREATE TABLE IndexWord (
            word TEXT PRIMARY KEY
        ); 
    """)
    db.execute("""
        CREATE TABLE Posting (
            word TEXT NOT NULL,
            documentName TEXT NOT NULL,
            frequency INTEGER NOT NULL,
            indexes TEXT NOT NULL,
            PRIMARY KEY(word, documentName),
            FOREIGN KEY (word) REFERENCES IndexWord(word)
        );
    """)
elif action == 'delete-db':
    db.close()
    remove('../inverted-index.db')
elif action == 'query':
    print("Vnesi poizvedbo: ")
    query = input()
    start = time.time()
    tokens = processString(query)
    tokens = ["'" + x + "'" for x in tokens]
    dbquery = db.execute('SELECT * FROM Posting WHERE word in ({s}) ORDER by frequency DESC'.format(s=','.join(tokens)))
    formated = {}
    frequencies = []
    for item in dbquery.fetchall():
        if item[1] not in formated:
            formated[item[1]] = {}
        formated[item[1]][item[0]] = [item[2], item[3]]
    for item in formated:
        if len(formated[item]) == len(tokens):
            fq = 0
            for key in formated[item]:
                fq += formated[item][key][0]
            frequencies.append((fq, item))
    processResults(frequencies, formated)
elif action == 'query-noindex':
    print("Vnesi poizvedbo: ")
    query = input()
    start = time.time()
    queryTokens = processString(query)
    manual = []

    folders = [f for f in listdir(mypath) if isdir(join(mypath, f))]
    for folder in folders:
        files = [file for file in listdir(mypath + '/' + folder)]
        for file in files:
            if ".html" in file:
                documentName = folder + "/" + file
                print("Searching file: " +  documentName + (" "*20),end="\r", flush=True)
                openedFile = getTextFromPath(mypath + "/" + documentName)
                tokens = processString(openedFile)
                frequencies = {x:tokens.count(x) for x in queryTokens}
                for key, value in frequencies.items():
                    occurences = list(findIndexes(key, openedFile.lower()))
                    occurences = [str(x) for x in occurences]
                    if frequencies[key] != 0:
                        manual.append([str(key), documentName, str(len(occurences)), ','.join(occurences)])
    formated = {}
    frequencies = []
    for item in manual:
        if item[1] not in formated:
            formated[item[1]] = {}
        formated[item[1]][item[0]] = [item[2], item[3]]
    for item in formated:
        if len(formated[item]) == len(queryTokens):
            fq = 0
            for key in formated[item]:
                fq += int(formated[item][key][0])
            frequencies.append((fq, item))
    processResults(frequencies, formated)
else:
    print()
    print("Napacna/manjkajoca opcija. Na voljo so: ")
    print("- build-db (ustvari bazo in doda tabele)")
    print("- delete-db (izbrise bazo iz diska)")
    print("- build-index (zgradi index iz datotek)")
    print("- query (poizvedba po indexu)")
    print("- query-noindex (klasicna poizvedba brez indeksa)")

end = time.time()
print()
print("Cas izvajanja: " + "{0:.2f}".format((end-start)*1000) + " ms")
db.close()