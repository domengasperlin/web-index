# Instructions 

This guide will get you up and running with indexer project.

# Clone the repo
Before starting, you need to clone the repo and move to folder indexer.
```bash
git clone https://domengasperlin@bitbucket.org/domengasperlin/web-index.git
cd web-index/indexer
```

# Install pip3 dependencies
In order to run the app, some Python dependencies needs to be installed. Nltk is used for tokenizing, tabulate is used for pretty printing the results, bs4 is used for removing HTML tags from source files and lxml is a document parser for bs4.
```bash
pip3 install nltk
pip3 install tabulate
pip3 install bs4
pip3 install lxml
```

# Install punkt
In order to use nltk stopwords, punkt has to installed. This can be achieved with a few Python commands in the CLI:
```python
python3
>>> import nltk
>>> nltk.download('punkt')
>>> exit()
```


# Creating db and generate index
After installing all prerequisites, indexer can now be run. Before making queries, index needs to be built. This is achieved by running commands build-db (creates empty SQLite database and tables) and build-index (builds index from source pages).
```bash
python3 indexer.py build-db 
python3 indexer.py build-index # this might take some time
```

# Querying
With index created, queries can now be made. There are two types of querying: with index or without index. After issuing this command, you need to input your query.

```bash
python3 indexer.py query
python3 indexer.py query-noindex
```

# Other commands
Besides mentioned commands, there is also a command for deleting the database.
```bash
python3 indexer.py delete-db
```

